import time
import grovepi
 
#Sensor connected to A0 Port
sensor = 2              # Pin 14 is A0 Port.
grovepi.pinMode(sensor,"INPUT")
while True:
    try:
        sensor_value = grovepi.analogRead(sensor)
 
        print ("sensor_value = %d" %sensor_value)
        time.sleep(.5)
 
    except IOError:
        print ("Error")