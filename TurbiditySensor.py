from time import sleep


import Adafruit_ADS1x15
adc = Adafruit_ADS1x15.ADS1115(address=0x48, busnum=1)

# Gain = 2/3 for reading voltages from 0 to 6.144V.
# See table 3 in ADS1115 datasheet
# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
GAIN = 1

# Main loop.
while 1:
    value = [0]
    # Read ADC channel 0
    value[0] = adc.read_adc(0, gain=GAIN)
    # Ratio of 15 bit value to max volts determines volts
    volts = value[0] / 32767.0 * 4.096
    # Tests shows linear relationship between psi & voltage:
    psi = 50.0 * volts - 25.0
    # Bar conversion
    #bar = psi * 0.0689475729

    print("{0:0.3f}V [{1}]".format(volts, value[0]))
  #  print("\n{0:0.0f} psi {1:0.1f} bar".format(psi, bar))
    
    sleep(1)